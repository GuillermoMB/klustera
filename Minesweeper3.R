#Use parallel libraries for parallelize
library(foreach)
library(doSNOW)

#Get parallel workers with normal
getDoParWorkers()

#Make parallel with doSNOW in a 4 core machine
#We have to specifiy the number of cores

registerDoSNOW(makeCluster(4,type="SOCK"))

getDoParWorkers()

library(RCurl)

#d <- read.csv(url('http://klustera.com/data/KW_AGOSTO-2.csv'), header=TRUE)

#d <- read.csv('C:/Users/Cindy Cordero/Documents/Dominos/DS2/II/KW.csv',header=TRUE, skip=1)

t <- read.csv('/Users/isragaytan/Documents/projects/Klustera/repobit/klustera', header=TRUE, skip =1,
              nrow = length(readLines('/Users/isragaytan/Documents/projects/Klustera/repobit/klustera')) - 7)

minesweeper(d)


# Function to explore the data
# file:     file name
#
# Example:    minesweeper("file")
#

minesweeper <- function(file) 
{
   library(stringr)
  
  title <- ("Estado de la palabra clave, Palabra clave, Campa?a, Grupo de anuncios, Estado, 
     CPC m?x., Clics, Impresiones, CTR, CPC medio, Coste, Posic. media, Conv. (1 por clic), 
     Coste/conv. (1 por clic), Porcentaje de conversiones (1 por clic), Conv. con vista, 
     Etiquetas, Nivel de calidad, CPC para la primera p?gina, Conversiones post-clic, Semana,
     URL de destino, Clics de contribuci?n, Impresiones de contribuci?n, 
     Conversiones post-impresi?n")

  title2 <- ("Week, Keyword state, Keyword, Campaign, Ad group, Status, Max. CPC, Clicks, 
     Impressions, CTR, Avg. CPC, Cost, Avg. position, Conv. (1-per-click), Cost / conv. (1-per-click),
     Conv. rate (1-per-click), View-through conv., Destination URL, Quality score,  
     First page CPC, Assist Clicks, Assist Impr., Click Assisted Conv., Impr. Assisted Conv.")

  #### Verificar funcionamiento ####
  if(length(t$Network..with.search.partners.) > 0) {
    tt2 <- t #Hace una copia
    t <- subset(t, t$Network..with.search.partners. != "Display.Network")
    ncoltt <- 25
    } else if (length(t$Red..con.partners.de.b?squeda.) > 0){
      tt2 <- t #Hace una copia
      t <- subset(t, t$Red..con.partners.de.b?squeda. != "Partners de b?squeda")
      ncoltt <- 25
    } else {
    t <- t
    ncoltt <- 24
  }
  
  
  d <- as.data.frame(matrix(0, ncol = ncoltt, nrow = nrow(t)))

#We use the foreach library in order to optimize and execute the script in parallelinstal   
library(foreach)   
  
   #Implement for each and dopar 
   
  foreach(i in 1:length(names(t))) %dopar%{
    if((names(t)[i] == "Semana") || (names(t)[i] == "Week")){
      ttSem <- 1
      colnames(d) <- c("Semana", "EstadoKW", "KW", "Campa?a", "Anuncios", "Estado", "CPCmax",
                       "Clics", "Impresiones", "CTR", "CPCmedio", "Coste", "PosMedia", 
                       "ConvClic", "CosteConv", "PorcConv", "ConvVista", "URL", "Calidad", 
                       "CPC1a", "ClicsCont", "ImpCont", "ConvPostClic", "ConvPostImp")
      if(names(t)[i] == "Semana"){
        d$Semana <- t$Semana
      } 
      if(names(t)[i] == "Week"){
        d$Semana <- t$Week
      }   
      break
    } else {
      ttSem <- 0
      colnames(d) <- c("EstadoKW", "KW", "Campa?a", "Anuncios", "Estado", "CPCmax", "Clics", 
                       "Impresiones", "CTR", "CPCmedio", "Coste", "PosMedia", "ConvClic", 
                       "CosteConv", "PorcConv", "ConvVista", "Etiquetas", "Calidad", "CPC1a",
                       "URL", "ClicsCont", "ImpCont", "ConvPostImp", "ConvPostClic")
      #Son las columnas que hay que cambiar el formtato
      ch <- c(6, 10, 11, 12, 14, 19)
    }
  }
  
  if((str_detect(title, names(t)[2]) == TRUE) || str_detect(title, names(t)[5]) == TRUE){
    idioma <- "Esp"
    d$Clics <- t$Clics
    d$ConvClic <- t$Conv...1.por.clic.
    d$ConvVista <- t$Conv..con.vista
    d$ConvPostClic <- t$Conversiones.post.clic
    d$Coste <- t$Coste
    d$CosteConv <- t$Coste.conv...1.por.clic.
    d$CPCmedio <- t$CPC.medio
    d$CTR <- t$CTR
    d$Estado <- t$Estado
    d$EstadoKW <- t$Estado.de.la.palabra.clave
    d$Anuncios <- t$Grupo.de.anuncios
    d$Impresiones <- t$Impresiones
    d$Calidad <- t$Nivel.de.calidad
    d$KW <- t$Palabra.clave
    d$PorcConv <- t$Porcentaje.de.conversiones..1.por.clic.
    d$PosMedia <- t$Posic..media
    d$URL <- t$URL.de.destino
   
    if(length(t$Campa?a) > 0){
      d$Campa?a <- t$Campa?a
        } else {
          if(length(t$Campa?.a) > 0){
            d$Campa?a <- t$Campa?.a
          }
        }
    if(length(t$CPC.m?x.) > 0){
      d$CPCmax <- t$CPC.m?x.
    } else {
      if(length(t$CPC.m?.x.) > 0){
        d$CPCmax <- t$CPC.m?.x.
      }
    }
    if(length(t$CPC.para.la.primera.p?gina) > 0){
      d$CPC1a <- t$CPC.para.la.primera.p?gina
    } else {
      if(length(t$CPC.para.la.primera.p?.gina) > 0){
        d$CPC1a <- t$CPC.para.la.primera.p?.gina
      }
    }       
    if(length(t$Clics.de.contribuci?n) > 0){
      d$ClicsCont <- t$Clics.de.contribuci?n
    } else {
      if(length(t$Clics.de.contribuci??n) > 0){
        d$ClicsCont <- t$Clics.de.contribuci??n
      }
    }   
    if(length(t$Impresiones.de.contribuci?n) > 0){
      d$ImpCont <- t$Impresiones.de.contribuci?n
    } else {
      if(length(t$Impresiones.de.contribuci??n) > 0){
        d$ImpCont <- t$Impresiones.de.contribuci??n
      }
    }     
    if(length(t$Conversiones.post.impresi?n) > 0){
      d$ConvPostImp <- t$Conversiones.post.impresi?n
    } else {
      if(length(t$Conversiones.post.impresi??n) > 0){
        d$ConvPostImp <- t$Conversiones.post.impresi??n
      }
    }     
    if(length(t$Semana) > 0){
      d$Semana <- t$Semana
      ch <- c(7, 11, 12, 13, 15, 20)
    } 
    if(length(t$Etiquetas) > 0){
      d$Etiquetas <- t$Etiquetas
    }       
  } else {
    idioma <- "Ing"
    d$Semana <- t$Week
    d$EstadoKW <- t$Keyword.state
    d$KW <- t$Keyword
    d$Campa?a <- t$Campaign
    d$Anuncios <- t$Ad.group
    d$Estado <- t$Status
    d$CPCmax <- t$Max..CPC
    d$Clics <- t$Clicks
    d$Impresiones <- t$Impressions
    d$CTR <- t$CTR    
    d$CPCmedio <- t$Avg..CPC
    d$Coste <- t$Cost
    d$PosMedia <- t$Avg..position
    d$ConvClic <- t$Conv...1.per.click.
    d$CosteConv <- t$Cost...conv...1.per.click.
    d$PorcConv <- t$Conv..rate..1.per.click.  
    d$ConvVista <- t$View.through.conv.
    d$URL <- t$Destination.URL
    d$Calidad <- t$Quality.score
    d$CPC1a <- t$First.page.CPC
    d$ClicsCont <- t$Assist.Clicks
    d$ImpCont <- t$Assist.Impr.
    d$ConvPostClic <- t$Click.Assisted.Conv.
    d$ConvPostImp <- t$Impr..Assisted.Conv.
    ch <- c(7, 11, 12, 13, 15, 20)
  }
  idioma

  library(plyr)
  library(ggplot2)
  options(digits = 2)
  
  dim(d) 
  nrow <- nrow(d)
  ncol <- ncol(d)
  names <- names(d)
  
  #summary(d)

  #Change the format of dates
  if (ttSem == 1){
    d$Semana <- as.character(d$Semana)
    d$Semana <- as.Date(d$Semana, "%d/%m/%Y")
  }

  ggplot(d, aes(Semana, Impresiones)) + geom_line() +
   # scale_x_date(format = "%b-%Y") + 
    xlab("") + ylab("Impresiones Diarias")
  
  
  # to know the class of each variable
  for (i in 1:ncol){
    cl <- class(d[,i])
    nam <- names[i]
    print(c(i, nam, cl))
  }
  
  # change format ',' to '.' and '%' to decimals
  d$CPCmax2 <- as.numeric(d$CPCmax)
  d$CPCmedio2 <- as.numeric(d$CPCmedio)
  d$Coste2 <- as.numeric(d$Coste)
  d$PosMedia2 <- as.numeric(d$PosMedia)
  d$CosteConv2 <- as.numeric(d$CosteConv)
  d$CPC1a2 <- as.numeric(d$CPC1a)
  d$CPCdif <- as.numeric(d$CPCmax)
  d$ConvVista <- as.factor(d$ConvVista)
  
  k <- 25
  
  for (j in ch) {
    if(class(d[,j]) == 'numeric'){
      d[,k] <- d[,j]
    } else {
    for (i in 1:nrow) {
      if(d[i,j] == ' --'){
        d[i,k] = 0 } else {
          a <- d[i,j]
          aa <- str_replace(a, "([%])", "")
          aaa <- str_replace(aa, "([.])", "")
          d[i,k] <- str_replace(aaa, "([,])", ".")
          #d[i,k] <- as.double(format(d[i,k],nsmall=2))
        }
    }  
    d[,k] <- as.numeric(d[,k])
    }
    k <- k + 1
  }
  
  d$CPCdif <- d$CPCmax2 - d$CPCmedio2

  if (idioma == "Ing"){
  ch2 <- c(12, 15)
  k <- 27
  for (j in ch2) {
      for (i in 1:nrow) {
        if(d[i,j] == ' --'){
          d[i,k] = 0 } else {
            a <- d[i,j]
            aa <- str_replace(a, "([%])", "")
            aaa <- str_replace(aa, "([,])", "")
            #d[i,k] <- str_replace(aaa, "([.])", ".")
            d[i,k] <- as.numeric(aaa)
          }
      }  
      d[,k] <- as.numeric(d[,k])
      k = k + 2
    }
  }
  
  d$Log <- as.numeric(d$ConvClic)
  
  for(h in 1:nrow(d)) {
    d$Log[h] = 0
    if(d$ConvClic[h] == 0) {
      d$Log[h] = 0
    } else {
      d$Log[h] = 1 
    }
  }
  
  d$Branded <- as.numeric(d$ConvVista)
  
  message('Declarar a continuaci?n la marca:')
  brand <- "utel"
  
  for(h in 1:nrow(d)) {
    if(str_detect(d$KW[h],brand) == TRUE) {
      d$Branded[h] = 1
    } else {
      d$Branded[h] = 0 
    }
  }
  
  #Create a copy of d
  dd <- d
  
  if (ttSem == 1){
  #Aggregate original data frame without considering dates.
    d <- ddply(d, .(Campa?a, Anuncios, KW, Estado, Log, Branded, Calidad), summarise, Clics = sum(Clics), 
             Impresiones = sum(Impresiones),  CPCmedio = weighted.mean(CPCmedio2, Clics), 
             Coste = sum(Coste2), PosMedia = weighted.mean(PosMedia2, Impresiones), 
             CosteConv = weighted.mean(CosteConv2, ConvVista), ClicsCont = sum(ClicsCont), 
             ImpCont = sum(ImpCont), ConvPostImp = sum(ConvPostImp), ConvPostClic = sum(ConvPostClic), 
             CPCdif = weighted.mean(CPCdif,Clics), CPCmax = weighted.mean(CPCmax2,Impresiones),
             ConvClic = sum(ConvClic), Fecmin = min(Semana), Fecmax = max(Semana))
  
    dd2 <- d
  } else {
    dd2 <- d
  }
  
  #par(mfrow = c(1,2), oma = c(0,0,2,0))
  Convirtio <- as.factor(d$Log)
  
  ggplot(data = d, aes(x = Campa?a, y = Impresiones, fill = Convirtio)) + geom_bar(stat = "identity", 
             position = position_dodge()) + ggtitle("Distribuci?n Impresiones - Convirti?")
  
  t1 <- tapply(d$Impresiones, d$Campa?a, mean)
  t2 <- tapply(d$Calidad, d$Campa?a, min)
  t3 <- tapply(d$Clics/d$Impresiones, d$Campa?a, median)
  
  a1 <- array(c(t1,t2,t3), dim = c(length(levels(d$Campa?a)),3), dimnames = list(levels(d$Campa?a), c("ImpMean", "MinCalidad", "MedianCTR")))
  ftable(format(a1, scientific = FALSE))
 summary(dat)

  library(ggplot2)
  qplot(PosMedia, data = d, geom = "histogram") + geom_density() #+ title("Histograma de Posici?n Media")
  qplot(Clics, Coste, geom = c("point"), data = d)
  ggplot(d, aes(x = Calidad, colour = Campa?a)) + geom_density()
  ggplot(d, aes(x = Calidad)) + geom_density()

  # Crear el pdf de las gr?ficas

  #pdf("reporte_minesweeper.pdf", paper = "a4")
  #d_ply(d, .(KW), function(x) plot(x$Coste2, x$PosMedia2, type = "l", 
  #main = unique(x$KW)))
  #dev.off()

#plot(dat)

# Study correlations between variables:
  library(reshape2)
  cor <- cor(dat)
  ncolcor <- ncol(cor)
  qplot(x = Var2, y = Var1, data = melt(cor(dat)), fill = value, geom = "tile", 
      main = "Diagrama de correlaci?n")


# To receive a data frame containing the names of the pair of variables with 
# correlation greater than 0.6

  k = 0
  print("Los pares de variables con correlaci?n >= 0.6 son")
  for (i in 1:ncolcor) {
    for(j in 1:ncolcor) {
      if ((cor[i,j] >= 0.6) && (j > i)){
        list <- print(c(names(dat[i]), names(dat[j]), cor[i,j]))
      } 
    }
  }
    


}